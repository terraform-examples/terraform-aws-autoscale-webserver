variable "server_port" {
  type        = "string"
  default     = "8080"
  description = "Sets the port for BusyBox to listen on"
}

variable "instance_type" {
  type        = "string"
  default     = "t2.micro"
  description = "Instance type to be used in launch configuration"
}
