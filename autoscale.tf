resource "aws_security_group" "web_server" {
  name = "tf-example-web-server"

  ingress {
    from_port   = "${var.server_port}"
    to_port     = "${var.server_port}"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "example_web_server" {
  image_id        = "${data.aws_ami.ubuntu.id}"
  instance_type   = "${var.instance_type}"
  security_groups = ["${aws_security_group.web_server.name}"]
  user_data       = <<EOT
#!/bin/bash
echo "Hello, World" > index.html
nohup busybox httpd -f -p ""${var.server_port}"" &
EOT

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "example" {
  launch_configuration = "${aws_launch_configuration.example_web_server.id}"
  availability_zones   = "${data.aws_availability_zones.all.names}"

  load_balancers    = ["${aws_elb.example.name}"]
  health_check_type = "ELB"

  min_size = 2
  max_size = 10

  tag {
    key                 = "Name"
    value               = "terraform-asg-example"
    propagate_at_launch = true
  }
}